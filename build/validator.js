
function determinePort() {

    let sftp = document.getElementById('sftphost').value.trim().split(".")[0];
    let portIn = document.getElementById('sftpport');
    let porterr = document.getElementById('porterr');

    if(!portIn.value) {
        if(sftp === "ftp") {
            portIn.value = '21';
            return;
        } else if(sftp === "sftp") {
            portIn.value = '22';
            return;
        }
    } else {
        if(sftp === "ftp" && portIn.value !== '21') {
            portIn.style.outline = "1px solid yellow";
            porterr.innerHTML = 'Unusual port for ftp. Are you sure this is correct?';
            return;
        } else if(sftp === "sftp" && portIn.value !== '22') {
            portIn.style.outline =  "1px solid yellow";
            porterr.innerHTML = 'Unusual port for sftp. Are you sure this is correct?';
            return;
        } else if((sftp === "sftp" && portIn.value === '22') || (sftp === "ftp" && portIn.value === '21')) {
            portIn.style.outline = "0px solid transparent";
            porterr.innerHTML = '';
            return;
        }
    }
}