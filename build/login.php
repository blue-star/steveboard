<?php
if(!isset($_SESSION)) {
    session_start();
}

echo '
    <head>
        <style>
            div {
                margin-bottom: 10px;
            }
            label {
                display: inline-block;
                width: 110px;
                color: #777777;
            }
            input {
                padding: 5px 10px;
            }
            .err {
                color: RED;
            }
        </style>
    </head>

        <h3>Login</h3>
        <br />
        <form id="login" name="login" method="post" action="database.php?method=login">
        <div>
            <label for="email">E-mail</label>
            <input type="text" id="email" name="email">
        </div>
        <div>
            <label for="password">Password</label> 
            <input type="password" id="password" name="password">
        </div>
        <div>
        <button type="submit" form="login" value="login">Log in</button>
     '
;

if(isset($_SESSION['login-failed'])) {
    echo '<br/> <p class="err"> Login failed : Incorrect email/password</p>';
}