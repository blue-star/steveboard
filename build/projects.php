<?php
if(!isset($_SESSION)) {
    session_start();
}
if(!isset($_SESSION['userid'])) {
    header("Location: index.php");
    die();
}

include 'database.php';

$all = GetProjectsForUser($_SESSION['userid']);

echo "<html><head>";
echo '<link rel="stylesheet" href="style.css"/>';
echo "</head><body>";

echo '<table>';
echo '<tr>
        <th>Name</th>
        <th>Live URL</th>
        <th>git URL</th>
        <th>Should update? </th>
    </tr>';
if(isset($all)) {
    foreach($all as $row) {
        echo '<tr>';
        $id = $row['id'];
        unset($row['id']);

        $i = 0;
        foreach($row as $cell) {
            echo '<td' 
            . ($i == 9? ' class="hidden"' : '') . '>' //If password field, apply 'hidden' css class
            . ($i == 1? '<a href="' . $cell . '" target="blank">' : '') //if Live URL field, linkify contents
            . ($i != 3? htmlspecialchars($cell) : '') //actual cell contents
            . ($i == 1? '</a>' : '') //end link if live url field
            ;
            if($i == 3) { // if commands field (should update for now)
            
                if($cell == null) 
                { echo htmlspecialchars('no'); }
                else 
                { echo htmlspecialchars('yes'); }
            }

            echo '</td>';
            $i++;
        }

        echo '<td>'
        . '<form id="inspect' .$id .'" method="post" action="database.php?method=inspectProject">'
        . '<input type="hidden" id="id" name="id" required value="' .$id .'">'
        . '<input type="submit" form="inspect' .$id .'" value = "inspect" />'
        . '</form>'
        . '<form id="reschedule'. $id.'" method="post" action="database.php?method=shouldUpdate">'
        . '<input type="hidden" id="should_update" name="should_update" required value="1">'
        . '<input type="hidden" id="id" name="id" required value="' .$id .'">'
        . '<input type="submit" form="reschedule' .$id .'" value= "reschedule" />'
        . '</form>'
        . '</td>'
        . '</tr>';
    } 
}
echo '</table>';

echo '<br /> <form id="create" action="create.php">
    <input type="submit" form="create" value="Create new entry" />
    </form>';

echo '</body></html>';