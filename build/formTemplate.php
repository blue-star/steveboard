<?php

function getTemplate($data, $operation, $title) {
    echo '
        <head>
            <style>
            div {
                margin-bottom: 10px;
            }
            label {
                display: inline-block;
                width: 110px;
                color: #777777;
            }
            input {
                padding: 5px 10px;
            }
            </style>
        </head>


        <h3>' .$title .'</h3><br />
        <form id="' .$operation .'" name="' .$operation .'" method="post" action="database.php?method=' .$operation .'">
            <div>
            <label for="name">project name</label>
            <input type="text" id="name" placeholder="Name of Project" name="name" required value="' .GetOrDefault($data, 'name') .'">
            </div>
            <div>
            <label for="liveurl">Live url</label>
            <input type="url" id="liveurl" name="liveurl" placeholder="https://example.com" required value="' .GetOrDefault($data, 'liveurl')  .'">
            </div>
            <div>
            <label for="giturl">Git url</label>
            <input type="url"  id="giturl" name="giturl" placeholder="https://gitexample.com/ex.git" required value="' .GetOrDefault($data, 'giturl') .'">
            </div>
            <div>
            <label for="gitbranch">Git branch</label>
            <input type="text" id="gitbranch" name="gitbranch" placeholder="master" required value="' .GetOrDefault($data, 'gitbranch') .'">
            </div>
            <div>
            <label for="buildscript">Build script</label>
            <input type="text" id="buildscript" name="buildscript" placeholder="build" value="' .GetOrDefault($data, 'buildscript') .'">
            </div>
            <div>
            <label for="gitbuildfolder">Git build folder</label>
            <input type="text" id="gitbuildfolder" name="gitbuildfolder" placeholder="build" required value="' .GetOrDefault($data, 'gitbuildfolder') .'">
            </div>
            <div>
            <label for="sftphost">(s)ftp host</label>
            <input type="text" placeholder="ftp.example.com" onblur="determinePort(this)" pattern="^([s]?ftp.)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$" title="(s)ftp host should include (sftp). (sftp.example.com)" id="sftphost" name="sftphost" required value="' .GetOrDefault($data, 'sftphost') .'">
            </div>
            <div>
            <label for="sftpport">(s)ftp port</label>
            <input type="text" id="sftpport" placeholder="21(ftp), 22(sftp)" onblur="determinePort(this)" name="sftpport" required value="' .GetOrDefault($data, 'sftpport') .'">
            <span id="porterr" style="color: rgb(125,125,0)"></span>
            </div>
            <div>
            <label for="sftusername">(s)ftp username</label>
            <input type="text" id="sftpusername" placeholder="user" name="sftpusername" required value="' .GetOrDefault($data, 'sftpusername') .'">
            </div>
            <div>
            <label for="sftppassword">(s)ftp password</label>
            <input type="password" id="sftppassword" placeholder="password" name="sftppassword" required value="' .GetOrDefault($data, 'sftppassword') .'">
            </div>
            <div>
            <label for="sftpdirectory">(s)ftp target directory</label>
            <input type="text" id="sftpdirectory" placeholder="/http/examplefolder/" name="sftpdirectory" required value="' .GetOrDefault($data, 'sftpdirectory') .'">
            </div>
            <div>
            <label for="webhook">Optional discord webhook</label>
            <input type="text" id="webhook" placeholder="Optional Discord Webhook" name="webhook" value="' .GetOrDefault($data, 'webhook') .'">
            </div>
            <input type="hidden" id="userid" name="userid" required value="' .GetOrDefault($_SESSION, 'userid') .'">
            <input type="hidden" id="id" name="id" value="' .GetOrDefault($data, 'id') .'">
        </form>
        <button type="back" form="back" Onclick="history.back()" value="1">Back</button>
        <button type="submit" form="' .$operation .'" value="' .$operation .'">' .$operation .'</button>
    ';
}

function GetOrDefault($array, $key)
{
  return isset($array[$key])? $array[$key] : "";
}