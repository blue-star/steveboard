<?php
if(!isset($_SESSION)) {
    session_start();
}

if(isset($_GET['method'])) {
    if($_GET['method'] == "create") {
        $_SESSION['POST'] = $_POST;
        CreateProject();
        unset($_SESSION['POST']);

        header("Location: projects.php");
        die();
    }
    else if($_GET['method'] == "login") {
        unset($_SESSION['login-failed']);

        Login();
    }

    else if($_GET['method'] == "inspectProject") {
        unset($_SESSION['INSPECT']);
        $_SESSION['INSPECT'] = GetProject($_POST['id']);
        header("Location: inspect.php");
        die();
    }

    else if($_GET['method'] == "update") {
        UpdateProject();

        $_SESSION['INSPECT'] = GetProject($_POST['id']);
        header("Location: projects.php");
        die();
    }

    else if($_GET['method'] == "delete") {
        DeleteProject();

        unset($_SESSION['INSPECT']);
        header("Location: projects.php");
        die();
    }

    else if($_GET['method'] == "shouldUpdate") {
        UpdateProjectShouldUpdate($_POST['id'], $_POST['should_update']);
        header("Location: projects.php");
        die();
    } 
}

function Login() {
    $sql = 'SELECT * FROM `users` WHERE `email` = (?)';
    $binding = [];
    array_push($binding, $_POST['email']);

    $result = executeQuery($sql, "s", $binding);

    if($result ->num_rows == 1) {
        $result = $result ->fetch_assoc();
        if(password_verify($_POST['password'], $result['password'])) {
            $_SESSION['userid'] = $result['id'];
            session_write_close();
            header("Location: index.php");
            die();
        }
    }
    $_SESSION['login-failed'] = "F";
    session_write_close();
    
    header("Location: login.php");
    die();
}

//probably fine to leave this as a native query, as we don't parse in any data
function GetProjects()
{
    require '../steveboardconfig/dbconfig.php';
    // Create connection
    $conn = new mysqli($DBServerName, $DBUserName, $DBPassword, $DBName);
    // Check connection
    if ($conn->connect_error) 
    {
        var_dump($conn);
        die("SteveConnection failed: " . $conn->connect_error . $DBServerName . "a");
    }

    $sql = "SELECT * FROM `projects`";
    $result = $conn->query($sql);

    $all = [];

    if ($result->num_rows > 0) 
    {
        // output data of each row
        while($row = $result->fetch_assoc()) 
        {
            array_push($all, $row);
        }
    } 
    $conn->close();

    return $all;
}

    //  Query: the prepared statement in string format
    //  Types: the type of the database column (i=int,s=string,d=double,b=blob)
    //  Binding: Array of binding values
function executeQuery($query, $types, $binding) {
    require '../steveboardconfig/dbconfig.php';
    // Create connection
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT); // error reporting
    $conn = new mysqli($DBServerName, $DBUserName, $DBPassword, $DBName);
    $conn->set_charset('utf8mb4');
    // Check connection
    if ($conn->connect_error) 
    {
        var_dump($conn);
        die("SteveConnection failed: " . $conn->connect_error . $DBServerName . "a");
    }
    
    $preparedStatement = $conn->prepare($query);
    if($types && $binding) {
        $preparedStatement->bind_param($types, ...$binding);
    }
    $preparedStatement->execute();
    $result = $preparedStatement->get_result();

    $conn -> close();

    return $result;
}

function GetProjectsForUser($user) {
    $sql = "SELECT `id`, `name`, `liveurl`, `giturl`, `commands`  FROM `projects` where `userid`= ?"; 
    $binding = [];
    array_push($binding, $user);
    $result = executeQuery($sql, "i", $binding);

    $all = [];

    if ($result->num_rows > 0) 
    {
        // output data of each row
        while($row = $result->fetch_assoc()) 
        {
            array_push($all, $row);
        }
    } 

    return $all;

}

function GetProject($id) {
    $sql = "SELECT * FROM `projects` where `id`= (?) and `userid` = (?)";
    $binding = [];
    array_push($binding, $id);
    array_push($binding, $_SESSION['userid']);
    $result = executeQuery($sql, "ii", $binding);

    if($result->num_rows == 1) {
        $result = $result ->fetch_assoc();

        return $result;
    }
    header("Location: projects.php");
}

function CreateProject() {

    //first value is 0 to trigger the auto-increment.
    //alternative would be to specify all column names
    $sql = "INSERT INTO `projects`(`id`, `name`, `liveurl`, `giturl`, `gitbranch`,`buildscript`, `gitbuildfolder`, 
                                   `sftphost`, `sftpport`, `sftpusername`, `sftppassword`,`sftpdirectory`,
                                   `webhook`, `userid`) VALUES (0,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    $binding = [];
    foreach($_POST as $key=>$val) {
        if($key == "id") {
            //do nothing, as this will be null
        } else {
            array_push($binding, $val);
        }
    }
    try {
        executeQuery($sql, "ssssssssssssi", $binding);
    } catch ( mysqli_sql_exception $e ) {
        if ($e->getCode() == 1062) {
            $_SESSION['POST']['err'] = "Error adding project: Duplicate entry!";
            header('location: create.php');
            die();
        } else {
            throw $e;
            die();
        }
    }
}

function UpdateProject() {
    //maybe a double check if user is authorized to update this?

    $sql = "UPDATE `projects` SET `name`=?, 
                                  `liveurl`=?,
                                  `giturl`=?,
                                  `gitbranch`=?,
                                  `buildscript`=?,
                                  `gitbuildfolder`=?,
                                  `sftphost`=?,
                                  `sftpport`=?,
                                  `sftpusername`=?,
                                  `sftppassword`=?,
                                  `sftpdirectory`=?,
                                  `webhook`=?
                WHERE `id`=?    ";
    $binding = [];
    $id;
    foreach($_POST as $key=>$val) {
        if($key == "id") {
            $id = $val;
        } else if($key != "userid") {
            array_push($binding, $val);
        }
    }
    array_push($binding, $id);

    executeQuery($sql, "ssssssssssssi", $binding);
}

function UpdateProjectShouldUpdate($id, $shouldUpdate) {
    $sql = "UPDATE `projects` SET `commands`=? where `id`=? ";
    $binding = [];
    array_push($binding, $shouldUpdate);
    array_push($binding, $id);

    executeQuery($sql, "si", $binding);
    header("Location: projects.php");
}

function DeleteProject() {
    // maybe a double check if user is authorized to delete this?

    $sql = "DELETE from `projects` where `id`=?";
    $binding = [];

    if(!isset($_POST["id"])) {
        header("Location: projects.php");
    }

    array_push($binding, $_POST["id"]);

    executeQuery($sql, "i", $binding);
}