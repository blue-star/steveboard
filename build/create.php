<?php
if(!isset($_SESSION)) {
    session_start();
}

require 'formTemplate.php';
echo '<script src="validator.js"> </script>';

$temp = [];
if(isset($_SESSION['POST'])) {
    $temp = $_SESSION['POST'];
    unset($_SESSION['POST']);
}
echo getTemplate($temp, 'create', 'Add a new project for Steve to handle');

if(isset($temp['err'])) {
    echo '<br /> <br />' .$temp['err'];
}