<?php
include 'database.php';
include '../steveboardconfig/apiconfig.php';

if($_SERVER['HTTP_KEY'] == $apikey) {
    $request_method = $_SERVER['REQUEST_METHOD'];

    if($request_method === 'GET') {
        echo json_encode(GetProjects());
    } else if($request_method === 'POST') {
        $_POST = json_decode(file_get_contents("php://input"), true);

        UpdateProjectShouldUpdate($_POST['id'], $_POST['should_update']);
    } else {
        header('HTTP/1.0 405 Method not Allowed');
    }
}
else
{
    header('HTTP/1.0 403 Forbidden');
}