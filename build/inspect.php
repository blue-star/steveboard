<?php
if(!isset($_SESSION)) {
    session_start();
}
if(!isset($_SESSION['userid'])) {
    header("Location: index.php");
    die();
}

$temp = [];
if(isset($_SESSION['INSPECT'])) {
    $temp = $_SESSION['INSPECT'];
}

require 'formTemplate.php';
echo '<script src="validator.js"> </script>';

echo '<script>
function ConfirmDelete()
{
  return confirm("Are you sure you want to delete?");
}
</script>';

echo getTemplate($temp, 'update', 'Project ' .$temp['name']);

echo '<br /> <br />'
. '<form id="delete" name="" method="post" action="database.php?method=delete">'
.   '<input type="hidden" id="id" name="id" value="'.  $temp['id'].'">'
. '</form>'
. '<button type="delete" form="delete" Onclick="return ConfirmDelete();" value="1">Delete</button>';

if(isset($temp['err'])) {
    echo '<br /> <br />' .$temp['err'];
}